const initialState = {
  isMenu0pen: false
}


const MenuReducer = (state, action) => {
    //Handle initial state
    if (typeof state === 'undefined') {
    return initialState
    }
    //return correct session depending on the action type
    switch (action.type) {
        case 'CHANGE_MENU_VISIBILITY':
                return (Object.assign({}, state, {
        isMenu0pen: !state.isMenu0pen
                    }))
        case 'SET_DISPLAY_SCREEN':
                return state
        case 'ADD_DUMMY_TASKS':
                return state
        //If action is not known, return current state    
        default:
        return state
                       
    }
}

export default MenuReducer