import React from 'react'
import PropTypes from 'prop-types'
import Checkbox from 'material-ui/Checkbox'
import {grey400} from 'material-ui/styles/colors';

const iconButtonElement = (
  <IconButton
    touch={true}
    tooltip="Task Options"
    tooltipPosition="bottom-left"
  >
    <MoreVertIcon color={grey400} />
  </IconButton>
);

const RightIconMenu () = (
  <IconMenu iconButtonElement={iconButtonElement}>
    <MenuItem onTouchTap={handleMenuAction("START_PROGRESS")}>Start Progress</MenuItem>
    <MenuItem onTouchTap={handleMenuAction("PAUSE_PROGRESS")}>Pause Progress</MenuItem>
    <MenuItem onTouchTap={handleMenuAction("RESTART_PROGRESS")}>Restart Timer</MenuItem>
    <Divider />
    <MenuItem onTouchTap={handleMenuAction("UPDATE_TASK_NAME")}>Update Task Name</MenuItem>
    <MenuItem onTouchTap={handleMenuAction("UPDATE_TASK_DURATION")}>Update Task Duration</MenuItem>
    <MenuItem onTouchTap={handleMenuAction("DELETE_TASK")}>Delete Task</MenuItem>
  </IconMenu>
);

const handleMenuAction = (actionType) => {
    switch (actionType)
            {
        case "START_PROGRESS":
            {}
        case "PAUSE_PROGRESS":
            {}
        case "RESTART_PROGRESS":
            {}
        case "UPDATE_TASK_NAME":
            {}
        case "UPDATE_TASK_DURATION":
            {}
        case "DELETE_TASK":
            {}
        default:
            {
                
            }
    }
}

const Task = ({ onCheck, taskName, remainingDuration, actionType }) => (
    
    <ListItem
          leftCheckbox={<Checkbox onCheck={onCheck} />}
          rightIconButton={<RightIconMenu  >}
          primaryText={taskName}
          secondaryText={"Remaining Time: " + remainingDuration + "hrs"}
    />
    
  <li
    onClick={onClick}
    style={{
      textDecoration: completed ? 'line-through' : 'none'
    }}
  >
    {text}
  </li>
)
/*
Todo.propTypes = {
  onClick: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
}

*/

export default Task