import React from 'react';
import {BarChart} from 'react-d3-basic';
import {Chart} from 'react-d3-core';
//import * as d3 from 'd3';


const data = [
  {
      "dayOfWeek": 'Monday',
      "tasksCompleted": 7
  },
  {
      "dayOfWeek": 'Tuesday',
      "tasksCompleted": 10
  },
  {
      "dayOfWeek": 'Wednesday',
      "tasksCompleted": 2
  },
  {
      "dayOfWeek": 'Thursday',
      "tasksCompleted": 6
  },
  {
      "dayOfWeek": 'Friday',
      "tasksCompleted": 23
  },
  {
      "dayOfWeek": 'Saturday',
      "tasksCompleted": 4
  },
  {
      "dayOfWeek": 'Sunday',
      "tasksCompleted": 9
  }
];

const chartSeries = [
  {
    field: 'tasksCompleted',
    name: 'Tasks Completed',
    color: '#ff7f0e'
  }
];

const title = 'Last Week History';
const width = 700;
const height = 300;
const margins = {left: 100, right: 100, top: 50, bottom: 50};
const xScale = 'ordinal';
const xLabel = "Day Of The Week";
const yLabel = "Total Completed tasks";
const yTicks = [2];

//const x = (d) =>  d.date;


//let parseDate = d3.time.format("%w").parse;
//let formatDay = d3.time.format("%a %d").parse;

//const xScale = 'time';

//const x = (d) =>  formatDay(d.date);
const x = (d) =>  d.dayOfWeek;



const ProductivityChart = () => (
    <div id="ProductivityChart_container">
        <h1> {title} </h1>
        <br />
        <BarChart
          title= {title}
          data= {data}
          width= {width}
          height= {height}
          chartSeries = {chartSeries}
          x= {x}
          xLabel= {xLabel}
          xScale= {xScale}
          yTicks= {yTicks}
          yLabel = {yLabel}
          margins = {margins}
        />
    </div>
);

export default ProductivityChart;