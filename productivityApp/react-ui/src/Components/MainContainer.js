import React from 'react'
import ProductivityChart from './ProductivityChart'
import TaskList from '../Containers/TaskList'

import MenuContainer from '../Containers/MenuContainer'

const MainContainer = () => (
  <div>
    <MenuContainer />
        <section id="MainContainer_container">
                  <ProductivityChart />
                  <TaskList />
        </section>
    <footer id="MainContainer_footer">&copy; Abraxas Copyrights 2017</footer>
  </div>
)

export default MainContainer