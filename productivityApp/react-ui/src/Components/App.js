import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import MainContainer from './MainContainer'
import '../Styles/App.css'
import MenuReducer from '../Reducers/MenuReducer'

//Create store
let store = createStore(MenuReducer)

//Use Provider to make the store available to all container components in the application without passing it explicitly.
const App = () => (
  <MuiThemeProvider>
    <Provider store={store}>
        <MainContainer />
    </Provider>
  </MuiThemeProvider>
)

export default App
