import React from 'react'
import {AppBar,Drawer,MenuItem} from 'material-ui'
import PropTypes from 'prop-types'

const MenuComponent = ({isMenu0pen,setMenuVisibility, showSelectedScreen,fillDummyTasks}) => (
        <div>
            <Drawer
                  docked={false}
                  width="30%"
                  open={isMenu0pen}
                  onRequestChange={(open) => setMenuVisibility()}
                >
                    <AppBar onLeftIconButtonTouchTap={
                        e => {
                        e.preventDefault()
                        setMenuVisibility()}}  />
                    <MenuItem onTouchTap={() => showSelectedScreen("PRODUCTIVITY_CHART")}>Productivity Chart</MenuItem>
                    <MenuItem onTouchTap={() => showSelectedScreen("TASK_LIST")}>Task List</MenuItem>
                    <MenuItem onTouchTap={() => fillDummyTasks({})}>Fill Dummy Tasks</MenuItem>
            </Drawer> 
            <header>
                <AppBar title={'Productivity App'} onLeftIconButtonTouchTap={
                        e => {
                        e.preventDefault()
                        setMenuVisibility()}} />
            </header>
        </div>
)

MenuComponent.propTypes = {
  isMenu0pen: PropTypes.bool.isRequired,
  setMenuVisibility: PropTypes.func.isRequired,
  showSelectedScreen: PropTypes.func.isRequired,
  fillDummyTasks: PropTypes.func.isRequired
}

export default MenuComponent