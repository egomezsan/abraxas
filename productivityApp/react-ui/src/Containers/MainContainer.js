import React from 'react';
import {AppBar,Drawer,MenuItem,IconButton} from 'material-ui'
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import ProductivityChart from '../Presentational/ProductivityChart'
import TaskList from './TaskList'
import {connect} from 'react-redux'

export default class MainContainer extends React.Component {
    
  constructor(props) {
    super(props);
    this.state = {open: false,
                 selectedOption: ''};
  }
    
    handleToggle = () => this.setState({open: !this.state.open})
    handleMenuNavigation = () => this.setState({open: false})
    title = 'Productivity App'

  render = () =>(
      <div>
        <Drawer
          docked={false}
          width="30%"
          open={this.state.open}
          onRequestChange={(open) => this.setState({open})}
        >
            <AppBar onLeftIconButtonTouchTap={this.handleToggle}  />
            <MenuItem onTouchTap={this.handleMenuNavigation}>New Task</MenuItem>
            <MenuItem onTouchTap={this.handleMenuNavigation}>Completed Tasks</MenuItem>
            <MenuItem onTouchTap={this.handleMenuNavigation}>Fill Random Tasks</MenuItem>
        </Drawer>
        <header>
              <AppBar title={this.title} onLeftIconButtonTouchTap={this.handleToggle} />
        </header>
        <section id="MainContainer_container">
                  <ProductivityChart />
                  <TaskList />
        </section>
        <footer id="MainContainer_footer">&copy; Abraxas Copyrights 2017</footer>
      </div>
    );
}

