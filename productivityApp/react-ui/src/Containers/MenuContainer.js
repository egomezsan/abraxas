import { connect } from 'react-redux'
import { setMenuVisibilityAction, fillDummyTasksAction,showSelectedScreenAction } from '../Actions/MenuActions'
import MenuComponent from '../Components/MenuComponent'

//const mapStateToProps = (state, ownProps) => {
//  return {
//    active: ownProps.filter === state.visibilityFilter
//  }
//}
//isMenu0pen,setMenuVisibility, handleMenu, showSelectedScreen,fillDummyTasks
const mapStateToProps = state => {
  return {
    isMenu0pen: state.isMenu0pen
  }
}

//closeMenu, showSelectedScreen,fillDummyTasks

const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    setMenuVisibility: () => {
      dispatch(setMenuVisibilityAction())
    },
    showSelectedScreen: () => {
      dispatch(showSelectedScreenAction(ownProps.selectedScreen))
    },
    fillDummyTasks: () => {
      dispatch(fillDummyTasksAction(ownProps.dummyTasks))
    }
  }
}

const MenuContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuComponent)

export default MenuContainer