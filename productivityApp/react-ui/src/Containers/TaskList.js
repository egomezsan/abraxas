import React from 'react';
import {List,ListItem} from 'material-ui/List';
import IconButton from 'material-ui/IconButton';
import MobileTearSheet from '../Components/MobileTearSheet';
import Subheader from 'material-ui/Subheader';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Divider from 'material-ui/Divider';
import {grey400, darkBlack, lightBlack} from 'material-ui/styles/colors';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import SelectField from 'material-ui/SelectField';

const DurationFilter = (props) => (

    <SelectField
          floatingLabelText="Filter by Duration"
          value={props.value}
          onChange={() => (alert('Changed'))}
          autoWidth={true}
        >
          <MenuItem value={1} primaryText="Short" />
          <MenuItem value={2} primaryText="Medium" />
          <MenuItem value={3} primaryText="Large" />
    </SelectField>
);



//Icon Element component
const iconButtonElement = (
  <IconButton
    touch={true}
    tooltip="Task Options"
    tooltipPosition="bottom-left"
  >
    <MoreVertIcon color={grey400} />
  </IconButton>
);

const rightIconMenu = (
  <IconMenu iconButtonElement={iconButtonElement}>
    <MenuItem>Start Progress</MenuItem>
    <MenuItem>Pause Progress</MenuItem>
    <MenuItem>Restart Timer</MenuItem>
    <Divider />
    <MenuItem>Update Description</MenuItem>
    <MenuItem>Delete Task</MenuItem>
  </IconMenu>
);

const Task = (props) => (

    <ListItem
          leftCheckbox={<Checkbox onCheck={(event,isInputChecked) => (alert('Task completed'))} />}
          rightIconButton={rightIconMenu}
          primaryText={props.name}
          secondaryText={"Remaining Time: " + props.remainingTime + "hrs"}
        />
);

const TaskList = () => (
    <div id="TaskList_container">
    
<MobileTearSheet>
      <DurationFilter value={1} />
      <Divider />
      <List>
        <Subheader>Pending Tasks</Subheader>
            <Task name="Correr" remainingTime= {12}/>
            <Task name="Leer" remainingTime= {30} />
            <Task name="Estudiar" remainingTime= {23} />
            <Task name="Bailar" remainingTime= {50} />
            <Task name="Trabajar" remainingTime= {120} />
            <Task name="Nadar" remainingTime= {5} />
      </List>
    </MobileTearSheet>
    </div>
    );

export default TaskList;