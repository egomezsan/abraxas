import React from 'react'
import {render} from 'react-dom'
import './Styles/index.css'
import App from './Components/App'
import registerServiceWorker from './Util/registerServiceWorker'
import injectTapEventPlugin from 'react-tap-event-plugin'

// Needed for onTouchTap (must be before the rendering)
injectTapEventPlugin()

//Render main function of the main component (App) using div app from the HTML
render(<App />,
    document.getElementById('app'))

//For performance
registerServiceWorker()