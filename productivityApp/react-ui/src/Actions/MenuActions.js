export const setMenuVisibilityAction = () => {
  return {
    type: 'CHANGE_MENU_VISIBILITY'
  }
}

export const fillDummyTasksAction = dummyTasks => {
  return {
    type: 'ADD_DUMMY_TASKS',
    dummyTasks
  }
}

export const showSelectedScreenAction = selectedScreen => {
  return {
    type: 'SET_DISPLAY_SCREEN',
    selectedScreen
  }
}